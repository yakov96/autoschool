FROM ubuntu:18.04
LABEL maintainer="Yakov Sharygin <jasha3131@gmail.com>"

RUN apt-get update
RUN apt-get install -y nginx python3-pip && pip3 install uwsgi
RUN apt-get install -y python3-dev libpq-dev

COPY . /app
WORKDIR /app

RUN pip3 install -r requirements.txt
RUN python3 manage.py collectstatic --settings=${SETTINGS_FILE} --noinput

RUN python3 manage.py runserver 0:8080