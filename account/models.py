from django.contrib.auth.base_user import BaseUserManager
from django.contrib.auth.models import AbstractUser
from django.db import models
from rest_framework.authtoken.models import Token
from random import randint


class UserManager(BaseUserManager):
    # TODO is_confirm поменять на False
    def create_user(self, email, name=None, is_confirm=True, password=None, **kwargs):
        if not email:
            raise ValueError('Необходимо указать Email')

        user = self.model(
            username=self.normalize_email(email),
            email=self.normalize_email(email),
            name=name,
            is_confirm=is_confirm
        )

        user.set_password(password)
        user.save(using=self._db)

        user_registration_confirm = UserRegistrationConfirm.objects.create(
            user=user
        )
        # user_registration_confirm.send_email()

        return user

    def create_superuser(self, email, password):
        user = self.create_user(
            email=email,
            password=password,
            is_confirm=True
        )
        user.is_superuser = True
        user.is_staff = True
        user.save(using=self._db)
        return user


class User(AbstractUser):
    name = models.CharField(verbose_name='Имя', max_length=200, null=True, blank=True)
    email = models.EmailField(verbose_name='Электронная почта', blank=False, unique=True)
    is_confirm = models.BooleanField(verbose_name='Подтвержден?', default=False)

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ()

    objects = UserManager()

    def __str__(self):
        return self.email

    def get_or_generate_token(self):
        return Token.objects.get_or_create(user=self)

    def refresh_token(self):
        token = self.get_or_generate_token()
        token.delete()
        return self.get_or_generate_token()

    class Meta:
        verbose_name = 'Аккаунт'
        verbose_name_plural = 'Аккаунты'


class UserRegistrationConfirm(models.Model):
    user = models.ForeignKey(User, verbose_name='Пользователь', on_delete=models.CASCADE)
    is_used = models.BooleanField(default=False)
    code = models.IntegerField(verbose_name='Код')

    class Meta:
        verbose_name = 'Запрос на подтверждение регистрации'
        verbose_name_plural = 'Запросы на подтверждение регистрации'

    @classmethod
    def generate_code(cls):
        return randint(10000, 99999)

    @classmethod
    def generate_test_code(cls):
        return 77777

    def save(self, *args, **kwargs):
        if not self.code:
            self.code = self.generate_test_code()

        super().save(*args, **kwargs)


User._meta.get_field('email')._unique = True
User._meta.get_field('email')._blank = False
User._meta.get_field('username')._unique = False
