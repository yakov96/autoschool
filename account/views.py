from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from rest_framework.authentication import TokenAuthentication
from rest_framework.response import Response
from rest_framework.views import APIView
from account.models import User
from account.serializers.user import RegistrationUserSerializer, UserSerializer
from autoschool.utils import CsrfExemptSessionAuthentication


@method_decorator(csrf_exempt, name='dispatch')
class RegistrationView(APIView):
    """
    Регистрация
    """

    authentication_classes = (CsrfExemptSessionAuthentication, TokenAuthentication)
    serializer_class = RegistrationUserSerializer

    def post(self, request):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        user = serializer.save()
        user.set_password(request.data.get('password'))

        return Response({
            'user': UserSerializer(instance=user).data
        }, status=201)


@method_decorator(csrf_exempt, name='dispatch')
class LoginView(APIView):
    """
    Авторизация
    """
    authentication_classes = (CsrfExemptSessionAuthentication, TokenAuthentication)

    def post(self, request):
        email = request.data.get('email', '')
        password = request.data.get('password', '')

        try:
            user = User.objects.get(email=email, is_confirm=True)
            if user.check_password(password):
                token, _ = user.get_or_generate_token()

                return Response({
                    'user': UserSerializer(instance=user).data,
                    'token': token.key
                }, status=200)

        except User.DoesNotExist:
            pass

        return Response({
            'non_field_error': 'Невозможно войти с предоставленными учетными данными',
            'errors': {
                'email': ['Поле заполнено неверно'],
                'password': ['Поле заполнено неверно']
            }
        }, status=400)
