from django.conf.urls import url
from structure.views.api_v1.group import GroupListAPIView, GroupDetailAPIView
from structure.views.api_v1.student import StudentListAPIView, StudentDetailAPIView
from structure.views.api_v1.teacher import TeacherListAPIView, TeacherDetailAPIView
from structure.views.api_v1.transport import TransportListAPIView, TransportDetailAPIView


urlpatterns = [
    url(r'^groups/(?P<group_pk>\d+)/students/$', StudentListAPIView.as_view(), name='group_student_list'),
    url(r'^groups/(?P<group_pk>\d+)/students/(?P<student_pk>\d+)/$', StudentDetailAPIView.as_view(),
        name='student_detail'),

    url(r'^groups/$', GroupListAPIView.as_view(), name='group_list'),
    url(r'^groups/(?P<pk>\d+)/$', GroupDetailAPIView.as_view(), name='group_detail'),

    url(r'^teachers/$', TeacherListAPIView.as_view(), name='teacher_list'),
    url(r'^teachers/(?P<pk>\d+)/$', TeacherDetailAPIView.as_view(), name='teacher_detail'),

    url(r'^transport/$', TransportListAPIView.as_view(), name='transport_list'),
    url(r'^transport/(?P<pk>\d+)/$', TransportDetailAPIView.as_view(), name='transport_detail'),
]
