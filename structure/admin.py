from django.contrib import admin
from structure.models import Student, Teacher, Transport, Group

admin.site.register(Student)
admin.site.register(Teacher)
admin.site.register(Transport)
admin.site.register(Group)
