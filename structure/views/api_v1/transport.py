from structure.models import Transport
from structure.serializers.api_v1.transport import TransportSerializer
from structure.views.api_v1.base import BaseListAPIView, BaseDetailAPIView


class TransportListAPIView(BaseListAPIView):
    model_class = Transport
    serializer_class = TransportSerializer


class TransportDetailAPIView(BaseDetailAPIView):
    model_class = Transport
    serializer_class = TransportSerializer
