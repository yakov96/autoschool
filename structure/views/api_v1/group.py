from structure.models import Group
from structure.serializers.api_v1.group import GroupSerializer
from structure.views.api_v1.base import BaseListAPIView, BaseDetailAPIView


class GroupListAPIView(BaseListAPIView):
    model_class = Group
    serializer_class = GroupSerializer


class GroupDetailAPIView(BaseDetailAPIView):
    model_class = Group
    serializer_class = GroupSerializer

