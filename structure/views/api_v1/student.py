from rest_framework import status
from rest_framework.generics import get_object_or_404
from rest_framework.response import Response

from structure.models import Student, Group
from structure.serializers.api_v1.student import StudentSerializer
from structure.views.api_v1.base import BaseListAPIView, BaseDetailAPIView


class StudentListAPIView(BaseListAPIView):
    model_class = Student
    serializer_class = StudentSerializer

    def get_queryset(self):
        return self.model_class.objects.filter(group=self.kwargs['group_pk'])

    def get_group_obg(self):
        return get_object_or_404(Group, pk=self.kwargs['group_pk'])

    def post(self, request, *args, **kwargs):
        """
        Создание студента, привязанного к группе
        """
        group = self.get_group_obg()

        serializer = self.serializer_class(data=request.data)
        if serializer.is_valid():
            serializer.save(group=group)
            return Response(serializer.data, status=status.HTTP_201_CREATED)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class StudentDetailAPIView(BaseDetailAPIView):
    model_class = Student
    serializer_class = StudentSerializer

    def get_object(self):
        obj = get_object_or_404(self.model_class, pk=self.kwargs['student_pk'], group=self.kwargs['group_pk'])
        return obj

