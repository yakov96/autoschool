from rest_framework.authentication import TokenAuthentication
from rest_framework.generics import get_object_or_404
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework import status
from autoschool.functions import CustomPagination
from autoschool.settings import PAGINATION_PAGE_SIZE


class BaseListAPIView(APIView, CustomPagination):
    """
    Базовый класс для методов получения списка и создания сущности

    get:
    Возвращает список сущностей
    post:
    Создание сущности
    """

    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)
    model_class = None
    serializer_class = None

    def get_queryset(self):
        return self.model_class.objects.all()

    def get(self, request, *args, **kwargs):
        """
        Получение списка объектов
        """
        page = request.GET.get('page', 1)
        page_size = request.GET.get('page_size', PAGINATION_PAGE_SIZE)

        CustomPagination.page_query_param = 'page'
        CustomPagination.page = page
        CustomPagination.page_size = page_size
        results = self.paginate_queryset(self.get_queryset(), request, view=self)
        serializer = self.serializer_class(instance=results, many=True)
        return self.get_paginated_response(serializer.data)

    def post(self, request, *args, **kwargs):
        """
        Создание нового экземпляра
        """
        serializer = self.serializer_class(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class BaseDetailAPIView(APIView):
    """
    Базовый класс для работы с сущностью
    """

    model_class = None
    serializer_class = None

    def get_object(self):
        obj = get_object_or_404(self.model_class, pk=self.kwargs['pk'])
        return obj

    def get(self, request, *args, **kwargs):
        """
        Получение экземпляра по ID
        """
        serializer = self.serializer_class(instance=self.get_object())
        return Response(serializer.data, status=status.HTTP_200_OK)

    def put(self, request, *args, **kwargs):
        """
        Редактирование экземпляра
        """
        serializer = self.serializer_class(instance=self.get_object(), data=request.data, partial=True)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, *args, **kwargs):
        """
        Удаление экземпляра
        """
        self.get_object().delete()
        return Response({
            'success': True
        }, status=status.HTTP_204_NO_CONTENT)
