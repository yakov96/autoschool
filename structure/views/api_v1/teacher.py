from structure.models import Teacher
from structure.serializers.api_v1.teacher import TeacherSerializer
from structure.views.api_v1.base import BaseListAPIView, BaseDetailAPIView


class TeacherListAPIView(BaseListAPIView):
    model_class = Teacher
    serializer_class = TeacherSerializer


class TeacherDetailAPIView(BaseDetailAPIView):
    model_class = Teacher
    serializer_class = TeacherSerializer
