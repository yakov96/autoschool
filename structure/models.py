from django.db import models


class GenderType:
    men = 'men'
    women = 'women'

    names = {
        men: 'Мужской',
        women: 'Женский'
    }

    choices = (
        (men, names[men]),
        (women, names[women]),
    )


class IdentificationType:
    passport = 'passport'
    military_cart = 'military_cart'
    birth_certificate = 'birth_certificate'
    international_passport = 'international_passport'
    foreigner_passport = 'foreigner_passport'

    names = {
        passport: 'Паспорт',
        military_cart: 'Военный билет',
        birth_certificate: 'Свидетельство о рождении',
        international_passport: 'Загранпаспорт',
        foreigner_passport: 'Паспорт иностранного гражданина'
    }

    choices = (
        (passport, names[passport]),
        (military_cart, names[military_cart]),
        (birth_certificate, names[birth_certificate]),
        (international_passport, names[international_passport]),
        (foreigner_passport, names[foreigner_passport]),
    )


class EducationType:
    secondary = 'secondary'
    secondary_special = 'secondary_special'
    incomplete_higher = 'incomplete_higher'
    higher = 'higher'

    names = {
        secondary: 'Среднее',
        secondary_special: 'Среднее специальное',
        incomplete_higher: 'Неоконченное высшее',
        higher: 'Высшее'
    }

    choices = (
        (secondary, names[secondary]),
        (secondary_special, names[secondary_special]),
        (incomplete_higher, names[incomplete_higher]),
        (higher, names[higher]),
    )


class DriverCategoryType:
    motorbike = 'motorbike'
    car = 'car'
    truck = 'truck'
    bus = 'bus'
    moped = 'moped'
    tram = 'tram'
    trolleybus = 'trolleybus'

    names = {
        motorbike: 'A',
        car: 'B',
        truck: 'C',
        bus: 'D',
        moped: 'M',
        tram: 'Tm',
        trolleybus: 'Tb'
    }

    choices = (
        (motorbike, names[motorbike]),
        (car, names[car]),
        (truck, names[truck]),
        (bus, names[bus]),
        (moped, names[moped]),
        (tram, names[tram]),
        (trolleybus, names[trolleybus]),
    )


class TeacherTypes:
    lecturer = 'lecturer'
    instructor = 'instructor'

    names = {
        lecturer: 'Лектор',
        instructor: 'Инструктор'
    }

    choices = (
        (lecturer, names[lecturer]),
        (instructor, names[instructor]),
    )


class TeacherStatusTypes:
    work = 'work'
    dismissed = 'dismissed'

    names = {
        work: 'Работает',
        dismissed: 'Уволен'
    }

    choices = (
        (work, names[work]),
        (dismissed, names[dismissed]),
    )


class Identification(models.Model):
    type = models.CharField(verbose_name='Тип', choices=IdentificationType.choices, max_length=200)
    series = models.CharField(verbose_name='Серия', max_length=200)
    number = models.CharField(verbose_name='Номер', max_length=200)
    institution = models.CharField(verbose_name='Кем выдано', max_length=250)
    issue_date = models.DateField(verbose_name='Дата выдачи')

    class Meta:
        verbose_name = 'Удостоверение личности'
        verbose_name_plural = 'Удостоверения личности'
        unique_together = ('series', 'number')

    def __str__(self):
        return '{0}: {1}'.format(self.series, self.number)


class MedicalCertificate(models.Model):
    series = models.CharField(verbose_name='Серия', max_length=200)
    number = models.CharField(verbose_name='Номер', max_length=200)
    issue_date = models.DateField(verbose_name='Дата выдачи')
    institution = models.CharField(verbose_name='Мед. учреждение', max_length=250)
    validity = models.DateField(verbose_name='Действует до', max_length=250)

    class Meta:
        verbose_name = 'Медицинская справка'
        verbose_name_plural = 'Медицинские справки'
        unique_together = ('series', 'number')

    def __str__(self):
        return '{0}: {1}'.format(self.series, self.number)


class DriverLicense(models.Model):
    series = models.CharField(verbose_name='Серия вод. удостоверения', max_length=200, null=True, blank=True)
    number = models.CharField(verbose_name='Номер вод. удостоверения', max_length=200, null=True, blank=True)
    organization_issue = models.CharField(verbose_name='Кем выдано', max_length=250, null=True, blank=True)
    category = models.CharField(verbose_name='Категория', choices=DriverCategoryType.choices, max_length=200,
                                null=True, blank=True)
    issue_date = models.DateField(verbose_name='Дата выдачи', null=True, blank=True)
    experience = models.CharField(verbose_name='Водительский стаж', max_length=200, null=True, blank=True)
    special_notes = models.CharField(verbose_name='Особые пометки', max_length=250, null=True, blank=True)

    class Meta:
        verbose_name = 'Водительское удостоверение'
        verbose_name_plural = 'Водительские удостоверения'
        unique_together = ('series', 'number')

    def __str__(self):
        return '{0}: {1}'.format(self.series, self.number)


class Transport(models.Model):
    brand = models.CharField(max_length=200, verbose_name='Марка')
    model = models.CharField(max_length=200, verbose_name='Модель')
    manufacture_year = models.PositiveSmallIntegerField(blank=True, null=True, verbose_name='Год выпуска')
    state_number = models.CharField(max_length=200, unique=True, verbose_name='Гос. номер')
    vin = models.CharField(max_length=200, verbose_name='VIN')
    color = models.CharField(max_length=200, verbose_name='Цвет')
    body_number = models.CharField(blank=True, max_length=200, null=True, verbose_name='Номер кузова')
    engine_number = models.CharField(blank=True, max_length=200, null=True, verbose_name='Номер двигателя')

    class Meta:
        verbose_name = 'Транспорт'
        verbose_name_plural = 'Транспорт'

    def __str__(self):
        return '{0}: {1}'.format(self.brand, self.model)


class Teacher(models.Model):
    first_name = models.CharField(verbose_name='Имя', max_length=200)
    middle_name = models.CharField(verbose_name='Отчество', max_length=200, null=True, blank=True)
    last_name = models.CharField(verbose_name='Фамилия', max_length=200)
    birth_day = models.DateField(verbose_name='Дата рождения')
    type = models.CharField(verbose_name='Тип', choices=TeacherTypes.choices, max_length=200)
    status = models.CharField(verbose_name='Статус', choices=TeacherStatusTypes.choices,
                              default=TeacherStatusTypes.work, max_length=200)
    phone = models.CharField(verbose_name='Телефон', max_length=200)
    street = models.CharField(verbose_name='Улица', max_length=200)
    house = models.CharField(verbose_name='Дом', max_length=200)
    apartment = models.CharField(verbose_name='Квартира', max_length=200, null=True, blank=True)
    transport = models.ForeignKey(Transport, verbose_name='Транспорт', blank=True, null=True, on_delete=models.SET_NULL)

    class Meta:
        verbose_name = 'Преподаватель'
        verbose_name_plural = 'Преподаватели'

    def __str__(self):
        return '{0}: {1}'.format(self.id, ' '.join([self.first_name, self.last_name]))


class Group(models.Model):
    title = models.CharField(verbose_name='Название', max_length=200)
    driver_category = models.CharField(verbose_name='Категория', choices=DriverCategoryType.choices, max_length=200)
    registration_date = models.DateField(verbose_name='Дата регистрации', auto_now_add=True)
    start_period = models.DateField(verbose_name='Дата начала обучения')
    end_period = models.DateField(verbose_name='Дата окончания обучения')
    order_number = models.CharField(verbose_name='№ приказа', max_length=200)
    order_date = models.DateField(verbose_name='Дата приказа')
    learning_start_date = models.DateField(verbose_name='Дата начала занятий по вождению')
    teachers = models.ManyToManyField(Teacher, verbose_name='Преподаватели', blank=True)

    class Meta:
        verbose_name = 'Учебная группа'
        verbose_name_plural = 'Учебные группы'

    def __str__(self):
        return '{0}: {1}'.format(self.id, self.title)


class Student(models.Model):
    first_name = models.CharField(verbose_name='Имя', max_length=200)
    middle_name = models.CharField(verbose_name='Отчество', max_length=200, null=True, blank=True)
    last_name = models.CharField(verbose_name='Фамилия', max_length=200)
    birth_day = models.DateField(verbose_name='Дата рождения')
    birth_place = models.CharField(verbose_name='Место рождения', max_length=200, null=True, blank=True)
    gender = models.CharField(verbose_name='Пол', choices=GenderType.choices, max_length=200)
    citizenship = models.CharField(verbose_name='Гражданство', max_length=200, null=True, blank=True)
    phone = models.CharField(verbose_name='Телефон', max_length=200)
    region = models.CharField(verbose_name='Регион', max_length=200)
    district = models.CharField(verbose_name='Район', max_length=200)
    locality = models.CharField(verbose_name='Населенный пунккт', max_length=200)
    street = models.CharField(verbose_name='Улица', max_length=200)
    house = models.CharField(verbose_name='Дом', max_length=200)
    apartment = models.CharField(verbose_name='Квартира', max_length=200, null=True, blank=True)

    identification = models.ForeignKey(Identification, verbose_name='Удостоверение личности',
                                       null=True, blank=True, on_delete=models.SET_NULL)
    medical_certificate = models.ForeignKey(MedicalCertificate, verbose_name='Медицинская справка',
                                            null=True, blank=True, on_delete=models.SET_NULL)

    old_driver_license = models.ForeignKey(DriverLicense, verbose_name='Водительское удостоверение',
                                           null=True, blank=True, on_delete=models.SET_NULL)

    education = models.CharField(verbose_name='Образование', max_length=200, choices=EducationType.choices,
                                 null=True, blank=True)
    work_place = models.CharField(verbose_name='Место работы', max_length=200, null=True, blank=True)
    mobile_phone = models.CharField(verbose_name='Мобильный телефон', max_length=200, null=True, blank=True)
    work_phone = models.CharField(verbose_name='Рабочий телефон', max_length=200, null=True, blank=True)
    email = models.EmailField(verbose_name='Email', max_length=200, null=True, blank=True)
    group = models.ForeignKey(Group, verbose_name='Группы', null=True, blank=True, on_delete=models.SET_NULL)
    education_cost = models.FloatField(verbose_name='Стоимость обучения', null=True, blank=True)
    remarks = models.TextField(verbose_name='Замечания', null=True, blank=True)

    class Meta:
        verbose_name = 'Курсант'
        verbose_name_plural = 'Курсанты'

    def __str__(self):
        return '{0}: {1}'.format(self.id, ' '.join([self.first_name, self.last_name]))
