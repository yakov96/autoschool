from rest_framework.serializers import ModelSerializer
from structure.models import Teacher
from structure.serializers.api_v1.transport import TransportSerializer


class TeacherSerializer(ModelSerializer):
    transport_obj = TransportSerializer(source='transport', read_only=True)

    class Meta:
        model = Teacher
        fields = 'id', 'first_name', 'middle_name', 'last_name', 'birth_day', 'type', 'status', 'phone', \
                 'street', 'house', 'apartment', 'transport', 'transport_obj',
