from rest_framework.serializers import ModelSerializer
from structure.models import Student
from structure.serializers.api_v1.group import GroupSerializer


class StudentSerializer(ModelSerializer):
    group_obj = GroupSerializer(source='group', read_only=True)

    class Meta:
        model = Student
        fields = 'id', 'first_name', 'middle_name', 'last_name', 'birth_day', 'birth_place', 'gender', \
                 'citizenship', 'phone', 'region', 'district', 'locality', 'street', 'house', 'apartment', \
                 'group', 'group_obj',
