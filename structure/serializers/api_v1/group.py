from rest_framework.serializers import ModelSerializer
from structure.models import Group


class GroupSerializer(ModelSerializer):
    class Meta:
        model = Group
        fields = 'id', 'title', 'driver_category', 'registration_date', 'start_period', 'end_period', 'order_number', \
                 'order_date', 'learning_start_date'
