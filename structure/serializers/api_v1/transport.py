from rest_framework.serializers import ModelSerializer
from structure.models import Transport


class TransportSerializer(ModelSerializer):
    class Meta:
        model = Transport
        fields = 'id', 'brand', 'model', 'manufacture_year', 'state_number', 'vin', 'color', \
                 'body_number', 'engine_number'
