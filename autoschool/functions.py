from rest_framework import pagination
from rest_framework.renderers import JSONRenderer
from rest_framework.response import Response


class UTF8CharsetJSONRenderer(JSONRenderer):
    charset = 'utf-8'


class CustomPagination(pagination.PageNumberPagination):
    ordering = 'datetime_create'

    def get_paginated_response(self, data):
        return Response({
            'links': {
                'next': self.get_next_link(),
                'previous': self.get_previous_link()
            },
            'success': True,
            'data': {
                'objects': data,
                'count': self.page.paginator.count,
                'pageCount': self.page.paginator.count
            }
        })
